This project was created during the 2017 AthenaHacks hackathon. 
I continued working on the project after, improving the accuracy and adding settings.
This was only tested on a Samsung Galaxy S4.

There are four tabs:
BPM - Measures heart rate.
      To use, cover the camera lens AND flash with your index finger. After some variable amount of time (chosen in the settings), your heart rate will be recorded.
	  This application is intended to use while measuring resting heart rate.
	  If your heart rate is higher than expected at rest for your chosen fitness level, you will be prompted with a breating exercise.
History - A log of heart rate measurements that were recorded
Breathe - A breathing exercise intended for relaxing
Settings - Settings to allow the user to choose sampling duration (the higher the more accurate), breathing exercise duration, and fitness level.