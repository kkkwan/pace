package com.example.kkkwan.pace;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelly on 4/9/2017.
 */

public class DBHelper extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 2;
    private static final String DATABASE_NAME = "myHeartRateDB";

    private static final String TABLE_HISTORY = "bpm_history";
    private static final String HISTORY_KEY_ID = "id";
    private static final String HISTORY_KEY_BPM = "bpm";
    private static final String HISTORY_KEY_DATE = "date";

    private static final String TABLE_SETTINGS = "pace_settings";
    private static final String SETTING_KEY_ID = "id";
    private static final String SETTING_KEY_SAMPLINGTIME = "sampling_time";
    private static final String SETTING_KEY_BREATHINGTIME = "breathing_time";
    private static final String SETTING_KEY_ACTIVITYLEVEL = "activity_level";

    private static final int DEFAULT_SAMPLING_TIME = 15;
    private static final int DEFAULT_BREATHING_TIME = 30;
    private static final int DEFAULT_ACTIVITY_LEVEL = 1;

    private Context mContext;
    private SQLiteDatabase mDb;

    public DBHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
//        this.mContext = context;
//        this.mDb = getWritableDatabase();
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_HISTORY_TABLE = "CREATE TABLE " + TABLE_HISTORY + "("
                + HISTORY_KEY_ID + " INTEGER PRIMARY KEY, "
                + HISTORY_KEY_BPM + " INTEGER, "
                + HISTORY_KEY_DATE + " TEXT" //YYYY-MM-DD HH:MM:SS
                + ")";
        db.execSQL(CREATE_HISTORY_TABLE);

        String CREATE_SETTINGS_TABLE = "CREATE TABLE " + TABLE_SETTINGS + "("
                + SETTING_KEY_ID + " INTEGER PRIMARY KEY, "
                + SETTING_KEY_SAMPLINGTIME + " INTEGER, "
                + SETTING_KEY_BREATHINGTIME + " INTEGER, "
                + SETTING_KEY_ACTIVITYLEVEL + " INTEGER"
                + ")";
        db.execSQL(CREATE_SETTINGS_TABLE);

        //TODO: Settings table
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_HISTORY);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_SETTINGS);
        onCreate(db);
    }

    public void BPMItem(BPMEntry entry) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(HISTORY_KEY_BPM, entry.getBPM());
        values.put(HISTORY_KEY_DATE, entry.getDateTime());
        db.insert(TABLE_HISTORY, null, values);
        db.close();
    }

    public void delBPMItem(BPMEntry entry) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_HISTORY, HISTORY_KEY_ID + " = ?", new String[]{Integer.toString(entry.getID())});
        db.close();
    }

    public List<BPMEntry> getAllHistory() {
        List<BPMEntry> entryList = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + TABLE_HISTORY + " ORDER BY datetime(" + HISTORY_KEY_DATE + ") ASC";
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                BPMEntry entry = new BPMEntry();
                entry.setID((Integer.parseInt(cursor.getString(0))));
                entry.setBPM(Integer.parseInt(cursor.getString(1)));
                entry.setDateTime(cursor.getString(2));
                entryList.add(entry);
            } while (cursor.moveToNext());
        }

        cursor.close();

        return entryList;
    }


    //save settings
    public void paceSetting(PaceSettings settings) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(SETTING_KEY_SAMPLINGTIME, settings.getSamplingTime());
        values.put(SETTING_KEY_BREATHINGTIME, settings.getBreathingTime());
        values.put(SETTING_KEY_ACTIVITYLEVEL, settings.getActivityLevel());

        db.update(TABLE_SETTINGS, values, "id = ?", new String[]{Integer.toString(settings.getID())});
        db.close();
    }

    public int getSamplingTime() {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT " + SETTING_KEY_SAMPLINGTIME + " FROM " + TABLE_SETTINGS + " WHERE id = ?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{"1"});

        if (cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        else {
            setDefaultSettings(db);

            return DEFAULT_SAMPLING_TIME; //default predefined sampling time
        }
    }

    public int getBreathingTime() {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT " + SETTING_KEY_BREATHINGTIME + " FROM " + TABLE_SETTINGS + " WHERE id = ?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{"1"});
        if (cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        else
        {
            setDefaultSettings(db);

            return DEFAULT_BREATHING_TIME; //default predefined breathing time
        }
    }

    public int getActivityLevel() {
        SQLiteDatabase db = this.getWritableDatabase();
        String selectQuery = "SELECT " + SETTING_KEY_ACTIVITYLEVEL + " FROM " + TABLE_SETTINGS + " WHERE id = ?";
        Cursor cursor = db.rawQuery(selectQuery, new String[]{"1"});
        if (cursor.moveToFirst()) {
            return cursor.getInt(0);
        }
        else
        {
            setDefaultSettings(db);
            return DEFAULT_ACTIVITY_LEVEL; //default predefined activity level
        }
    }

    public void setDefaultSettings(SQLiteDatabase db){
        ContentValues values = new ContentValues();
        values.put(SETTING_KEY_SAMPLINGTIME, DEFAULT_SAMPLING_TIME);
        values.put(SETTING_KEY_BREATHINGTIME, DEFAULT_BREATHING_TIME);
        values.put(SETTING_KEY_ACTIVITYLEVEL, DEFAULT_ACTIVITY_LEVEL);
        db.insert(TABLE_SETTINGS, null, values);
        db.close();

    }
}

