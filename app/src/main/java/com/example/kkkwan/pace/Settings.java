package com.example.kkkwan.pace;

import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.nfc.tech.NdefFormatable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatImageView;
import android.util.Log;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Kelly on 4/9/2017.
 */

public class Settings extends AppCompatActivity {

    private DBHelper db;

    private static AppCompatButton bpmButton;
    private static AppCompatButton historyButton;
    private static AppCompatButton breatheButton;
    private static AppCompatButton settingsButton;

    private static TextView samplingText;
    private static TextView breathingText;
    private static TextView activityText;

    private static SeekBar seekSampling;
    private static SeekBar seekBreathing;
    private static SeekBar seekActivity;

    boolean mWriteMode = false;
    private NfcAdapter mNfcAdapter;
    private PendingIntent mNfcPendingIntent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        samplingText = (TextView) findViewById(R.id.sampling_text);
        breathingText = (TextView) findViewById(R.id.breathing_text);
        activityText = (TextView) findViewById(R.id.activity_text);

        seekSampling = (SeekBar) findViewById(R.id.seek_sampling);
        seekBreathing = (SeekBar) findViewById(R.id.seek_breathing);
        seekActivity = (SeekBar) findViewById(R.id.seek_activity);

        //initialize settings
        samplingText.setText("Sampling Duration: " + getSettings().get(0) + " seconds");
        breathingText.setText("Breathing Duration: " + Math.round(((getSettings().get(1))/60.0)*10d)/10d + " minutes");
        String[] levels = {"Potato", "Average", "Athletic"};
        activityText.setText("Fitness Level: " + levels[getSettings().get(2)]);

        seekSampling.setProgress(getSettings().get(0));
        seekBreathing.setProgress(getSettings().get(1));
        seekActivity.setProgress(getSettings().get(2));

        seekSampling.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int duration = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                duration = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                samplingText.setText("Sampling Duration: " + duration + " seconds");
                setSettings(duration, getSettings().get(1), getSettings().get(2));
            }
        });

        seekBreathing.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int duration = 0;
            double minutes = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                duration = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                minutes = Math.round((duration/60.)*10d)/10d;
                breathingText.setText("Breathing Duration: " + minutes + " minute(s)");
//                setSettings(getSettings().get(0), (int)(minutes*60), 1);
                setSettings(getSettings().get(0), (int)(minutes*60), getSettings().get(2));

            }
        });

        seekActivity.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            int level = 0;

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                level = progress;
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                String[] levels = {"Potato", "Average", "Athletic"};
                activityText.setText("Fitness Level: " + levels[level]);
                setSettings(getSettings().get(0), getSettings().get(1), level);
            }
        });

        bpmButton = (AppCompatButton) findViewById(R.id.button_heart);
        historyButton = (AppCompatButton) findViewById(R.id.button_history);
        breatheButton = (AppCompatButton) findViewById(R.id.button_breathe);
        settingsButton = (AppCompatButton) findViewById(R.id.button_settings);

        settingsButton.setEnabled(false);
        settingsButton.setBackgroundColor(ContextCompat.getColor(this, R.color.neuBluePress));
        settingsButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        bpmButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), HeartRateActivity.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        historyButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), History.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        breatheButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Breathe.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Settings.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });

//        https://github.com/balloob/Android-NFC-Tag-Writer

        ((AppCompatButton) findViewById(R.id.write_to_nfc)).setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                mNfcAdapter = NfcAdapter.getDefaultAdapter(Settings.this);
                mNfcPendingIntent = PendingIntent.getActivity(Settings.this, 0,
                    new Intent(Settings.this, Settings.class).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);

                enableTagWriteMode();

                new AlertDialog.Builder(Settings.this).setMessage("Touch tag to write.").setTitle("Write app to NFC tag")
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        @Override
                        public void onCancel(DialogInterface dialog) {
                            disableTagWriteMode();
                        }
                }).create().show();
            }
        });
    }

    private void enableTagWriteMode() {
        mWriteMode = true;
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        IntentFilter[] mWriteTagFilters = new IntentFilter[] { tagDetected };
        mNfcAdapter.enableForegroundDispatch(this, mNfcPendingIntent, mWriteTagFilters, null);
    }

    private void disableTagWriteMode() {
        mWriteMode = false;
        mNfcAdapter.disableForegroundDispatch(this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        // Tag writing mode
        if (mWriteMode && NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {
            Tag detectedTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            NdefRecord appRecord = NdefRecord.createApplicationRecord(
                    "com.example.kkkwan.pace");
            NdefMessage message = new NdefMessage(appRecord);

            if (writeTag(message, detectedTag)) {
                Toast.makeText(this, "Success: Wrote placeid to nfc tag", Toast.LENGTH_LONG)
                        .show();
            }
        }
    }
    /*
    * Writes an NdefMessage to a NFC tag
    */
    public boolean writeTag(NdefMessage message, Tag tag) {
        int size = message.toByteArray().length;
        try {
            Ndef ndef = Ndef.get(tag);
            if (ndef != null) {
                ndef.connect();
                if (!ndef.isWritable()) {
                    Toast.makeText(getApplicationContext(),
                            "Error: tag not writable",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
                if (ndef.getMaxSize() < size) {
                    Toast.makeText(getApplicationContext(),
                            "Error: tag too small",
                            Toast.LENGTH_SHORT).show();
                    return false;
                }
                ndef.writeNdefMessage(message);
                return true;
            } else {
                NdefFormatable format = NdefFormatable.get(tag);
                if (format != null) {
                    try {
                        format.connect();
                        format.format(message);
                        return true;
                    } catch (IOException e) {
                        return false;
                    }
                } else {
                    return false;
                }
            }
        } catch (Exception e) {
            return false;
        }
    }

    public List<Integer> getSettings(){
        List<Integer> sampleBreatheActivityTrio = new ArrayList<Integer>();;
        db = new DBHelper(this);
        sampleBreatheActivityTrio.add(db.getSamplingTime());
        sampleBreatheActivityTrio.add(db.getBreathingTime());
        sampleBreatheActivityTrio.add(db.getActivityLevel());

        return sampleBreatheActivityTrio;
    }

    public void setSettings(int samplingTime, int breathingTime, int activityLevel){
        db = new DBHelper(this);
        db.paceSetting(new PaceSettings(1, samplingTime, breathingTime, activityLevel));
    }
}