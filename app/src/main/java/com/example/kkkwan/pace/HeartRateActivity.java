package com.example.kkkwan.pace;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.database.Cursor;
import android.hardware.Camera;
import android.hardware.Camera.PreviewCallback;
import android.os.Bundle;
import android.os.PowerManager;
import android.os.PowerManager.WakeLock;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Created by Kelly on 4/8/2017.
 */

public class HeartRateActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    private static final int POTATO_LEVEL = 90;
    private static final int AVERAGE_LEVEL = 80;
    private static final int ATHELETIC_LEVEL = 65;


    private DBHelper db;

    private SurfaceView preview = null;
    private SurfaceHolder holder = null;
    private Camera camera = null;
    private TextView rtext = null;
    private TextView gtext = null;
    private TextView btext = null;
    private TextView textH = null;

    private AppCompatButton bpmButton;
    private AppCompatButton historyButton;
    private AppCompatButton breatheButton;
    private AppCompatButton settingsButton;

    private AppCompatButton breatheButtonWarn;
    private Button toastButton;

    private WakeLock wakeLock = null;

    private long cameraCoveredStartTime = 0;
    private long cameraCoveredEndTime = 0;

    private int samplingTime = 0;
    private int activityLevel = 0;

    private double multiplier = 0;
    private int redThreshold = 150;
    private int darknessThreshold = 20;

    private List<Integer> redValues = new ArrayList<Integer>();
    private List<Integer> greenValues = new ArrayList<Integer>();
    private List<Integer> blueValues = new ArrayList<Integer>();
    private List<Integer> greyValues = new ArrayList<Integer>();
    private String temp = "";
    private int redBPM = 0;
    private int greenBPM = 0;
    private int blueBPM = 0;
    private int greyBPM = 0;
    private int thisBPM = 0;
    private double startTime = 2;
    private double currentTimer = 0;
    private int reset = 0;
    private boolean done = false;

    private DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private Date date = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_heart_rate);

        preview = (SurfaceView) findViewById(R.id.preview);
        holder = preview.getHolder();
        holder.addCallback(surfaceCallback);
        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        //rgbtext for debugging purposes
        rtext = (TextView) findViewById(R.id.rtext);
        gtext = (TextView) findViewById(R.id.gtext);
        btext = (TextView) findViewById(R.id.btext);
        textH = (TextView) findViewById(R.id.heart_rate);

        bpmButton = (AppCompatButton) findViewById(R.id.button_heart);
        historyButton = (AppCompatButton) findViewById(R.id.button_history);
        breatheButton = (AppCompatButton) findViewById(R.id.button_breathe);
        settingsButton = (AppCompatButton) findViewById(R.id.button_settings);

        breatheButtonWarn = (AppCompatButton) findViewById(R.id.button_breathe_warn);
        toastButton = (Button) findViewById(R.id.toast_button);

        bpmButton.setEnabled(false);
        bpmButton.setBackgroundColor(ContextCompat.getColor(this, R.color.neuBluePress));
        bpmButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        bpmButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), HeartRateActivity.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        historyButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), History.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        breatheButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Breathe.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Settings.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });

        breatheButtonWarn.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Breathe.class); //intent for specific component
                Bundle bundle = new Bundle();
                bundle.putString("message", "Your heart rate of " + thisBPM + " is higher than normal for your fitness level.");
                bundle.putString("message1", "Please follow the breathing exercise and try to relax.");
                bundle.putString("message2", "");
                intent.putExtras(bundle);

                startActivity(intent);

                finish();
            }
        });

        toastButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                date = new Date();
                insertEntry(thisBPM, dateFormat.format(date));
                Toast.makeText(getApplicationContext(), thisBPM + "bpm was recorded!", Toast.LENGTH_LONG).show();

                //TODO: why does the toast button look different?
            }
        });

        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        wakeLock = pm.newWakeLock(PowerManager.FULL_WAKE_LOCK, "DoNotDimScreen");

        //New settings, added 10/18/2017
        samplingTime = getSettings().get(0);
        activityLevel = getSettings().get(2);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onResume() {
        super.onResume();

        wakeLock.acquire();

        camera = Camera.open();
//        holder = preview.getHolder();
//        holder.addCallback(surfaceCallback);
//        holder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    @Override
    public void onPause() {
        super.onPause();

        wakeLock.release();

        camera.setPreviewCallback(null);
        camera.stopPreview();
        camera.release();
        camera = null;
    }

    //camera preview
    private PreviewCallback previewCallback = new PreviewCallback() {

        @Override
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Size size = camera.getParameters().getPreviewSize();
            if (data == null) throw new NullPointerException();
            if (size == null) throw new NullPointerException();

            int width = size.width;
            int height = size.height;

            Camera.Parameters parameters = camera.getParameters();

            //process the frames
            int[] avgRGB = ImageProcessing.avgRGBColors(data.clone(), height, width);
            int avgRed = avgRGB[0];
            int avgGreen = avgRGB[1];
            int avgBlue = avgRGB[2];
            int avgHue = ImageProcessing.getHue(avgRed, avgGreen, avgBlue);
            int avgGrey = ImageProcessing.avgGreyscale(data.clone(), height, width);

            //if the torch/light was just turned off, wait a moment before checking darkness
            //this is because the moment the light is off, RGB will be ~(1,1,1)
            //which will trigger the torch again immediately regardless of the actual darkness
            if(reset != 0){
                reset++;
                if(startTime * 10 <= reset){
                    reset = 0;
                }
            }
            else {
                //if the camera is covered, turn the torch/light on
                if(avgBlue < darknessThreshold && camera.getParameters().getFlashMode().equals("off")){
                    cameraCoveredStartTime = System.currentTimeMillis();
                    parameters.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                    camera.setParameters(parameters);
                    redValues.clear();
                    greenValues.clear();
                    blueValues.clear();
                    greyValues.clear();
                    currentTimer = 0;
                }
            }

            //if the torch is on, check if "it's a finger" by checking red
            if(camera.getParameters().getFlashMode().equals("torch")){
                cameraCoveredEndTime = System.currentTimeMillis();

                //first, clear the values that might be from flash
                if((cameraCoveredEndTime - cameraCoveredStartTime)/1000d > 0 && (cameraCoveredEndTime - cameraCoveredStartTime)/1000d < startTime) {
                    redValues.clear();
                    greenValues.clear();
                    blueValues.clear();
                    greyValues.clear();
                    done = false;
                }

                //if after flash, it's still "not a finger", then turn the flash off
                //otherwise we save that red value
                if((cameraCoveredEndTime - cameraCoveredStartTime)/1000d > startTime){
                    if(!ImageProcessing.isRed(avgRGB) || avgRed < redThreshold) {
                        parameters.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                        camera.setParameters(parameters);
                        reset++;
                        textH.setText("---");
                    }
                    else if((cameraCoveredEndTime - cameraCoveredStartTime)/1000d >= (samplingTime + startTime)){

                        if(!done) {
//                            rtext.setText("RED: " + redBPM + "\ntime: " + currentTimer + "\npeaks: " + countPeakValues(redValues));
                            done = true;

                            switch (activityLevel){
                                case 0:
                                    if (thisBPM > POTATO_LEVEL) {
                                        breatheButtonWarn.performClick();
                                    }
                                    break;
                                case 1:
                                    if (thisBPM > AVERAGE_LEVEL) {
                                        breatheButtonWarn.performClick();
                                    }
                                    break;
                                case 2:
                                    if (thisBPM > ATHELETIC_LEVEL) {
                                        breatheButtonWarn.performClick();
                                    }
                                    break;
                                default:
                                    break;
                            }


                            if (done) {
                                textH.setText(thisBPM + " bpm");

                                //Low pass filter added 10/18/2017 for better accuracy
                                //log data for testing
//                                logData(lowPassFilter(greyValues), greenValues, blueValues, greyValues);

                                //also does db insertion
                                toastButton.performClick();
                            }
                        }

                    }
                    else{
                        redValues.add(avgRed);
                        greenValues.add(avgGreen);
                        blueValues.add(avgBlue);
                        greyValues.add(avgGrey);

                        //Round to two decimal places
                        multiplier = Math.round(((60 / (((cameraCoveredEndTime - cameraCoveredStartTime) / 1000d) - startTime))) * 100d)/100d;

                        redBPM = (int)(countPeakValues(redValues) * multiplier);
                        greenBPM = (int)(countPeakValues(greenValues) * multiplier);
                        blueBPM = (int)(countPeakValues(blueValues) * multiplier);
                        greyBPM = (int)(countPeakValues(greyValues) * multiplier);
                        thisBPM = greyBPM; //whichever is more accurate, set as thisBPM to be recorded
//                        btext.setText("time: " + currentTimer + "\nmultiplier: " + multiplier);

                        //update bpm text every second
                        if((int)(Math.round((cameraCoveredEndTime - cameraCoveredStartTime)/1000d  - startTime)) > (int)currentTimer) {
                            currentTimer = (int)(Math.round((cameraCoveredEndTime - cameraCoveredStartTime)/1000d) - startTime);

                            textH.setText(thisBPM + " bpm");
                        }
                    }

                }



            }
//            boolean isAvgRed = ImageProcessing.isRed(avgRed, avgGreen, avgBlue);
//            int isAvgRed = ImageProcessing.getHue(avgRed, avgGreen, avgBlue);

            //updated in real time
//            gtext.setText("end: " + samplingTime + "\ntime: " + currentTimer +  "\nbeats: " + countPeakValues(greyValues) +  "\nmultiplier: " + multiplier + "\nRED: " + redBPM + "\nGREEN: " + greenBPM + "\nBLUE: " + blueBPM + "\nGREY: " + greyBPM );
//            btext.setText("RED: " + avgRed + "\nGREEN: " + avgGreen + "\nBLUE: " + avgBlue + "\nGREY: " + avgGrey);
        }
    };

    private SurfaceHolder.Callback surfaceCallback = new SurfaceHolder.Callback() {

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            try {
                camera.setPreviewDisplay(holder);
                camera.setPreviewCallback(previewCallback);
            } catch (Throwable t) {
            }
        }

        //Size of viewport changed (eg. screen rotation)
        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = getSmallestPreviewSize(parameters);

            parameters.setPreviewSize(size.width, size.height);
            camera.setDisplayOrientation(90);

            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
//            parameters.setFocusMode(Camera.Parameters.FOCUS_MODE_MACRO);
//            parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_INCANDESCENT);
//            parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_CLOUDY_DAYLIGHT);
//            parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_DAYLIGHT);
//            parameters.setWhiteBalance(Camera.Parameters.WHITE_BALANCE_FLUORESCENT);
            parameters.set("iso","800");
            camera.setParameters(parameters);
            camera.startPreview();
        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
        }
    };


    private static Camera.Size getSmallestPreviewSize(Camera.Parameters parameters) {
        int previewSizes = parameters.getSupportedPreviewSizes().size();
        return parameters.getSupportedPreviewSizes().get(previewSizes - 1);
    }

    private static int countPeakValues(List<Integer> dataSet){

        List<Integer> newDataSet = dataSet;
        if(dataSet.size() > 1) {
            newDataSet = lowPassFilter(dataSet);
        }

        int numPeaks = 0;
        for(int i = 1; i < newDataSet.size() - 1; i++){
            if(newDataSet.get(i) >= newDataSet.get(i-1) && newDataSet.get(i+1) < newDataSet.get(i)){
                numPeaks++;
            }
        }

        return numPeaks;
    }

    public static List<Integer> lowPassFilter(List<Integer> dataSet){
        double alpha = 0.15; //less than 0.5

        List<Integer> newDataSet = new ArrayList<Integer>();

        newDataSet.add(dataSet.get(0));

        for(int i = 1; i < dataSet.size() - 1; i++){
            newDataSet.add((int)(newDataSet.get(i-1) + alpha * (dataSet.get(i) - newDataSet.get(i-1)) ));
        }

        return newDataSet;
    }

    public void insertEntry(int bpm, String datetime){
        db = new DBHelper(this);
        db.BPMItem(new BPMEntry(0, bpm, datetime)); //id 0 is just a placeholder; BPMItem() will generate an id
    }

    public List<Integer> getSettings(){
        List<Integer> sampleBreatheActivityTrio = new ArrayList<Integer>();
        db = new DBHelper(this);
        sampleBreatheActivityTrio.add(db.getSamplingTime());
        sampleBreatheActivityTrio.add(db.getBreathingTime());
        sampleBreatheActivityTrio.add(db.getActivityLevel());

        return sampleBreatheActivityTrio;
    }

    //for testing purposes
    private static void logData(List<Integer> dataSet, List<Integer> dataSet2, List<Integer> dataSet3, List<Integer> dataSet4){
        for(int i = 0; i < dataSet.size(); i++) {
            Log.d("myLog:colorData", "" + dataSet.get(i) + " " + dataSet2.get(i) + " " + dataSet3.get(i) + " " + dataSet4.get(i) );
//            Log.d("myLog:peaks", "" + countPeakValues(dataSet4));
        }
    }
}