package com.example.kkkwan.pace;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by Kelly on 4/8/2017.
 */

public class ImageProcessing {

    public static int avgGreyscale(byte[] nv21, int width, int height) {
        int pixelCount = width * height;
        int[] out = new int[pixelCount];
        int sumGREY = 0;

        for (int i = 0; i < pixelCount; ++i) {
            int lum = nv21[i] & 0xFF;
            out[i] = Color.argb(0xFF, 0, lum, 0);

            sumGREY += out[i];
        }

        return sumGREY/pixelCount;
    }

    public static int[] avgRGBColors(byte[] yuv420sp, int width, int height) {
        int[] avgRGB = new int[3];

//        int[] rgbPixels = convertYUV420_NV21toRGB8888(yuv420sp, width, height);
        int[] rgbPixels = YUV_NV21_TO_RGB(yuv420sp, width, height);
//        Bitmap bmp = Bitmap.createBitmap(rgbPixels, 100, 100, Bitmap.Config.ARGB_8888);

        int sumRED = 0;
        int sumGREEN = 0;
        int sumBLUE = 0;

        for (int i = 0; i < rgbPixels.length; i++) {
            sumRED = sumRED + Color.red(rgbPixels[i]);
            sumGREEN = sumGREEN + Color.green(rgbPixels[i]);
            sumBLUE = sumBLUE + Color.blue(rgbPixels[i]);
        }

        avgRGB[0] = sumRED / rgbPixels.length;
        avgRGB[1] = sumGREEN / rgbPixels.length;
        avgRGB[2] = sumBLUE / rgbPixels.length;

        return avgRGB;
    }

//http://stackoverflow.com/questions/5272388/extract-black-and-white-image-from-android-cameras-nv21-format/12702836

    /**
     * Converts YUV420 NV21 to RGB8888
     *
     * @param data   byte array on YUV420 NV21 format.
     * @param width  pixels width
     * @param height pixels height
     * @return a RGB8888 pixels int array. Where each int is a pixels ARGB.
     */
    public static int[] convertYUV420_NV21toRGB8888(byte[] data, int width, int height) {
        int size = width * height;
        int offset = size;
        int[] pixels = new int[size];
        int u, v, y1, y2, y3, y4;

        // i percorre os Y and the final pixels
        // k percorre os pixles U e V
        for (int i = 0, k = 0; i < size; i += 2, k += 2) {
            y1 = data[i] & 0xff;
            y2 = data[i + 1] & 0xff;
            y3 = data[width + i] & 0xff;
            y4 = data[width + i + 1] & 0xff;

            u = data[offset + k] & 0xff;
            v = data[offset + k + 1] & 0xff;
            u = u - 128;
            v = v - 128;

            pixels[i] = convertYUVtoRGB(y1, u, v);
            pixels[i + 1] = convertYUVtoRGB(y2, u, v);
            pixels[width + i] = convertYUVtoRGB(y3, u, v);
            pixels[width + i + 1] = convertYUVtoRGB(y4, u, v);

            if (i != 0 && (i + 2) % width == 0)
                i += width;
        }

        return pixels;
    }

//http://stackoverflow.com/questions/12469730/confusion-on-yuv-nv21-conversion-to-rgb
    public static int[] YUV_NV21_TO_RGB(byte[] yuv, int width, int height) {
        final int size = width * height;
        int[] pixels = new int[size];

        final int ii = 0;
        final int ij = 0;
        final int di = +1;
        final int dj = +1;

        int a = 0;
        for (int i = 0, ci = ii; i < height; ++i, ci += di) {
            for (int j = 0, cj = ij; j < width; ++j, cj += dj) {
                int y = (0xff & ((int) yuv[ci * width + cj]));
                int v = (0xff & ((int) yuv[size + (ci >> 1) * width + (cj & ~1) + 0]));
                int u = (0xff & ((int) yuv[size + (ci >> 1) * width + (cj & ~1) + 1]));
                y = y < 16 ? 16 : y;

                int r = (int) (1.164f * (y - 16) + 1.596f * (v - 128));
                int g = (int) (1.164f * (y - 16) - 0.813f * (v - 128) - 0.391f * (u - 128));
                int b = (int) (1.164f * (y - 16) + 2.018f * (u - 128));

                r = r < 0 ? 0 : (r > 255 ? 255 : r);
                g = g < 0 ? 0 : (g > 255 ? 255 : g);
                b = b < 0 ? 0 : (b > 255 ? 255 : b);

                pixels[a++] = 0xff000000 | (r << 16) | (g << 8) | b;
            }
        }

        return pixels;
    }

//    private static int convertYUVtoRGB(int y, int u, int v) {
//        int r, g, b;
//
//        r = y + (int) (1.370705f * v);
//        g = y - (int) ((0.698001 * v) - (0.337633f * u));
//        b = y + (int) (1.732446f * u);
//        r = r > 255 ? 255 : r < 0 ? 0 : r;
//        g = g > 255 ? 255 : g < 0 ? 0 : g;
//        b = b > 255 ? 255 : b < 0 ? 0 : b;
//        return 0xff000000 | (b << 16) | (g << 8) | r;
//    }


    private static int convertYUVtoRGB(int y, int u, int v) {
        int r, g, b;

        r = y + (int) 1.402f * v;
        g = y - (int) (0.344f * u + 0.714f * v);
        b = y + (int) 1.772f * u;
        r = r > 255 ? 255 : r < 0 ? 0 : r;
        g = g > 255 ? 255 : g < 0 ? 0 : g;
        b = b > 255 ? 255 : b < 0 ? 0 : b;
        return 0xff000000 | (b << 16) | (g << 8) | r;
    }

//http://stackoverflow.com/questions/23090019/fastest-formula-to-get-hue-from-rgb

    public static int getHue(int red, int green, int blue) {

        float min = Math.min(Math.min(red, green), blue);
        float max = Math.max(Math.max(red, green), blue);

        float hue = 0f;
        if (max == red) {
            hue = (green - blue) / (max - min);

        } else if (max == green) {
            hue = 2f + (blue - red) / (max - min);

        } else {
            hue = 4f + (red - green) / (max - min);
        }

        hue = hue * 60;
        if (hue < 0) hue = hue + 360;

        return Math.round(hue);
    }


    public static boolean isRed(int red, int green, int blue) {
        float max = Math.max(Math.max(red, green), blue);

        if (max == red)
            return true;
        else
            return false;
    }

    public static boolean isRed(int[] rgb) {
        int red = rgb[0];
        int green = rgb[1];
        int blue = rgb[2];

        float max = Math.max(Math.max(red, green), blue);

        if (max == red)
            return true;
        else
            return false;
    }
}
