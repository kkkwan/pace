package com.example.kkkwan.pace;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

/**
 * Created by Kelly on 4/8/2017.
 */

public class HistoryAdapter extends ArrayAdapter<BPMEntry> {
    private DBHelper db;

    public HistoryAdapter(Context context, List<BPMEntry> entries) {
        super(context, 0, entries);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.bpm_entry, parent, false);
        }

        TextView bpmText = (TextView) convertView.findViewById(R.id.bpm_value);
        TextView dateTimeText = (TextView) convertView.findViewById(R.id.datetime);
        AppCompatButton deleteButton = (AppCompatButton) convertView.findViewById(R.id.delete_button);

        // Get the data item for this position
        final BPMEntry entry = getItem(position);

        bpmText.setText(String.valueOf(entry.getBPM()));
        dateTimeText.setText(entry.getDateTime());

        deleteButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                new AlertDialog.Builder(getContext())
                .setTitle("Delete Entry")
                .setMessage("Are you sure you want to delete this entry?")
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        deleteEntry(entry);

                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
//                        Toast.makeText(getContext(), "toast", Toast.LENGTH_LONG).show();
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
            }
        });
        // Return the completed view to render on screen
        return convertView;
    }

    public void deleteEntry(BPMEntry entry){
        db = new DBHelper(getContext());
        db.delBPMItem(entry);
        remove(entry);
        notifyDataSetChanged();
    }
}
