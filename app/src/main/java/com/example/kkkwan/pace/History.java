package com.example.kkkwan.pace;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.ListView;

import java.util.Collections;
import java.util.List;

/**
 * Created by Kelly on 4/8/2017.
 */

public class History extends AppCompatActivity {

    private DBHelper db;

    private static AppCompatButton bpmButton;
    private static AppCompatButton historyButton;
    private static AppCompatButton breatheButton;
    private static AppCompatButton settingsButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);


        bpmButton = (AppCompatButton) findViewById(R.id.button_heart);
        historyButton = (AppCompatButton) findViewById(R.id.button_history);
        breatheButton = (AppCompatButton) findViewById(R.id.button_breathe);
        settingsButton = (AppCompatButton) findViewById(R.id.button_settings);

        historyButton.setEnabled(false);
        historyButton.setBackgroundColor(ContextCompat.getColor(this, R.color.neuBluePress));
        historyButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        bpmButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), HeartRateActivity.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        historyButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), History.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        breatheButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Breathe.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Settings.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });


        ListView historyListView = (ListView) findViewById(R.id.history_list);
        List<BPMEntry> historyList = getHistory();
        Collections.reverse(historyList);
        HistoryAdapter hAdapter = new HistoryAdapter(this, historyList);
        historyListView.setAdapter(hAdapter);
    }

    public List<BPMEntry> getHistory(){
        db = new DBHelper(this);
        return db.getAllHistory();
    }

}
