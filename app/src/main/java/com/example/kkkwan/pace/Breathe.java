package com.example.kkkwan.pace;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.view.View;
import android.widget.TextView;

/**
 * Created by Kelly on 4/9/2017.
 */

public class Breathe extends AppCompatActivity {

    private static AppCompatButton bpmButton;
    private static AppCompatButton historyButton;
    private static AppCompatButton breatheButton;
    private static AppCompatButton settingsButton;

    private static TextView text = null;
    private static TextView text1 = null;
    private static TextView text2 = null;

    String message;
    String message1;
    String message2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_breathe);

        bpmButton = (AppCompatButton) findViewById(R.id.button_heart);
        historyButton = (AppCompatButton) findViewById(R.id.button_history);
        breatheButton = (AppCompatButton) findViewById(R.id.button_breathe);
        settingsButton = (AppCompatButton) findViewById(R.id.button_settings);

        breatheButton.setEnabled(false);
        breatheButton.setBackgroundColor(ContextCompat.getColor(this, R.color.neuBluePress));
        breatheButton.setTextColor(ContextCompat.getColor(this, R.color.white));
        bpmButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), HeartRateActivity.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        historyButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), History.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        breatheButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Breathe.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });
        settingsButton.setOnClickListener(new View.OnClickListener(){
            public void onClick(View v){
                Intent intent = new Intent(getApplicationContext(), Settings.class); //intent for specific component
                startActivity(intent);
                finish();
            }
        });


        text = (TextView) findViewById(R.id.text);
        text1 = (TextView) findViewById(R.id.text1);
        text2 = (TextView) findViewById(R.id.text2);

        //if Breathe activity started by HeartRateActivity, set messages that were passed down
        if(getIntent().getExtras() != null) {
            message = getIntent().getStringExtra("message");
            message1 = getIntent().getStringExtra("message1");
            message2 = getIntent().getStringExtra("message2");

            text.setText(message);
            text1.setText(message1);
            text2.setText(message2);
        }
        else{
            text.setText("");
            text1.setText("");
            text2.setText("");
        }

    }
}
