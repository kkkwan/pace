package com.example.kkkwan.pace;

/**
 * Created by Kelly on 4/8/2017.
 */

public class BPMEntry {
    private int id;
    private int bpm;
    private String datetime;

    public BPMEntry(){

    }

    public BPMEntry(int id, int bpm, String datetime){
        this.id = id;
        this.bpm = bpm;
        this.datetime = datetime;
    }

    public void setID(int id){
        this.id = id;
    }

    public void setBPM(int bpm){
        this.bpm = bpm;
    }

    public void setDateTime(String datetime){
        this.datetime = datetime;
    }

    public int getID(){
        return id;
    }

    public int getBPM(){
        return bpm;
    }

    public String getDateTime(){
        return datetime;
    }

}
