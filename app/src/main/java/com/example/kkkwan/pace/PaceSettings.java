package com.example.kkkwan.pace;

/**
 * Created by Kelly on 10/18/2017.
 */

public class PaceSettings {
    private int id;
    private int samplingTime;
    private int breathingTime;
    private int activityLevel;

    public PaceSettings(){

    }

    public PaceSettings(int id, int samplingTime, int breathingTime, int activityLevel){
        this.id = id;
        this.samplingTime = samplingTime;
        this.breathingTime = breathingTime;
        this.activityLevel = activityLevel;
    }

    public void setID(int id){
        this.id = id;
    }

    public void setSamplingTime(int samplingTime){
        this.samplingTime = samplingTime;
    }

    public void setBreatheTime(int breathingTime){
        this.breathingTime = breathingTime;
    }

    public void setActivityLevel(int activityLevel){
        this.activityLevel = activityLevel;
    }


    public int getID(){
        return id;
    }

    public int getSamplingTime(){
        return samplingTime;
    }

    public int getBreathingTime(){ return breathingTime; }

    public int getActivityLevel(){ return activityLevel; }

}
